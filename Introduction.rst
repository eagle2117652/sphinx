Installation
============

Prerequisites:
--------------

#. python: download latest version of python and check the box to add python to environment path `link <https://www.python.org/downloads/>`_ 
#. sphinx: after installing python install sphinx using the following command:

    .. code-block:: console

        pip install sphinx

#. read the docs theme: in the same manner install rtd theme

    .. code-block:: console

        pip install sphinx_rtd_theme