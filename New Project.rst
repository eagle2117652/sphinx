New Project
===========

In Command prompt run the following commands:

    .. code-block:: console

        //create a folder for the project:
        mkdir coding
        cd coding
        //start sphinx
        sphinx-quickstart
        //building the project
        make html 
        //if using Visual Code integrated terminal
        ./make html      