Restructured Text
=================

Create a new page:
------------------

* To create a new Restructured page right click in project folder within visual code and choose to create a new file <file name>.rst 
* Add a reference to tha page in index.rst by adding the <file name> after :caption: Contents: by pressing enter twice and make sure you have three spaces from the beginning of the line.

.. note:: 

    Tap will not work because it will create four spaces

.. image:: /Images/newPageReferance.PNG

Text Decorations:
-----------------

* To use *italic* use asterisk symbol between the required text, for **bold** user double asterisk and for code use two ``back `` ticks``  

Lists:
------

* use * to make bulleted list 
* Use #. to make numbered list 
* To create sub list press tap at the beginning of a new line 

`Cheat Sheet <http://openalea.gforge.inria.fr/doc/openalea/doc/_build/html/source/sphinx/rest_syntax.html>`_ 

caution, danger, tip, note 

Supported languages
Pygments supports an ever-growing range of languages. Watch this space...
Programming languages
ActionScript
Ada
ANTLR
AppleScript
Assembly (various)
Asymptote
Awk
Befunge
Boo
BrainFuck
C, C++
C#
Clojure
CoffeeScript
ColdFusion
Common Lisp
Coq
Cryptol (incl. Literate Cryptol)
Crystal
Cython
D
Dart
Delphi
Dylan
Elm
Erlang
Ezhil Ezhil - A Tamil programming language
Factor
Fancy
Fortran
F#
GAP
Gherkin (Cucumber)
GL shaders
Groovy
Haskell (incl. Literate Haskell)
IDL
Io
Java
JavaScript
Lasso
LLVM
Logtalk
Lua
Matlab
MiniD
Modelica
Modula-2
MuPad
Nemerle
Nimrod
Objective-C
Objective-J
Octave
OCaml
PHP
Perl
PovRay
PostScript
PowerShell
Prolog
Python 2.x and 3.x (incl. console sessions and tracebacks)
REBOL
Red
Redcode
Ruby (incl. irb sessions)
Rust
S, S-Plus, R
Scala
Scheme
Scilab
Smalltalk
SNOBOL
Tcl
Vala
Verilog
VHDL
Visual Basic.NET
Visual FoxPro
XQuery
Zephir
Template languages
Cheetah templates
Django / Jinja templates
ERB (Ruby templating)
Genshi (the Trac template language)
JSP (Java Server Pages)
Myghty (the HTML::Mason based framework)
Mako (the Myghty successor)
Smarty templates (PHP templating)
Tea
Other markup
Apache config files
Bash shell scripts
BBCode
CMake
CSS
Debian control files
Diff files
DTD
Gettext catalogs
Gnuplot script
Groff markup
HTML
HTTP sessions
INI-style config files
IRC logs (irssi style)
Lighttpd config files
Makefiles
MoinMoin/Trac Wiki markup
MySQL
Nginx config files
POV-Ray scenes
Ragel
Redcode
ReST
Robot Framework
RPM spec files
SQL, also MySQL, SQLite
Squid configuration
TeX
tcsh
Vim Script
Windows batch files
XML
XSLT
YAML

pygments.org/languages/