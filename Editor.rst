Editor
======

* Install Visual Studio Code: `Download <https://code.visualstudio.com>`_ 
* Right click on the project folder and choose to open with Code
* Open conf.py and change the htm_theme to equal 'sphinx_rtd_theme'
* Use the following code to build the project:

    .. code-block:: console

        ./make html
    
* In Visual Code market place install reStructuredText 
* After reloading Code you can preview the pages by clicking the preview button at the top or by clicking Crt+k->r 
