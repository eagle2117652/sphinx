.. Sphinx documentation master file, created by
   sphinx-quickstart on Wed Dec 13 20:44:17 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sphinx's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Introduction
   New Project
   Editor
   Restructured Text



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
